<!-- <button onclick="onLogout()">LOGOUT</button> -->
<div class="sidebar-container">
  <div id="sidebar" class="sidebar">
    <div>
      <button id="sidebar_btn">
        <img id="sidebar_btn_icon">
      </button>
    </div>
    <img class="img" src="{{ url('storage/icons/PACH.png') }}" class="mx-4" style="margin-left: 25px; width: 80%">
    <h6 class="parent-menu d-none w-100 text-white mx-2 my-4">PARENT</h6>
    <h6 class="admin-menu d-none w-100 text-white mx-2 my-4">ADMINISTRATION</h6>
    <h6 class="superadmin-menu d-none w-100 text-white mx-2 my-4">SUPERADMIN</h6>
    <a class="parent-menu d-none" href="/parent/dashboard/childrens/page">
      <img class="me-2" src="{{ url('storage/icons/user-smile-line.png') }}">
      <span>Children</span>
    </a>
    <a class="parent-menu d-none" href="/parent/dashboard/profile">
      <img class="me-2" src="{{ url('storage/icons/user-line.png') }}">
      <span>Profile</span>
    </a>
    <a class="admin-menu d-none" href="/officer/dashboard/vaccines/page">
      <img class="me-2" src="{{ url('storage/icons/syringe-line.png') }}">
      <span>Vaccine</span>
    </a>
    <!--
    <a class="admin-menu d-none" href="/officer/dashboard/questions/page">
      <img class="me-2" src="{{ url('storage/icons/draft-line.png') }}">
      <span>Question</span>
    </a>
    -->
    <a class="admin-menu d-none" href="/officer/dashboard/milestones/page">
      <img class="me-2" src="{{ url('storage/icons/clipboard-line.png') }}">
      <span>Milestone</span>
    </a>
    <a class="admin-menu d-none" href="/officer/dashboard/tips/page">
      <img class="me-2" src="{{ url('storage/icons/lightbulb-line.png') }}">
      <span>Parenting Tip</span>
    </a>
    <a class="admin-menu d-none" href="/officer/dashboard/profile">
      <img class="me-2" src="{{ url('storage/icons/user-line.png') }}">
      <span>Profile</span>
    </a>
    <a class="superadmin-menu d-none" href="/officer/dashboard/admins/page">
      <img class="me-2" src="{{ url('storage/icons/user-follow-line.png') }}">
      <span>Admin</span>
    </a>
    <a class="superadmin-menu d-none" href="/officer/dashboard/profile">
      <img class="me-2" src="{{ url('storage/icons/user-line.png') }}">
      <span>Profile</span>
    </a>
  </div>
</div>